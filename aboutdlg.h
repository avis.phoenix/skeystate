#ifndef ABOUTDLG_H
#define ABOUTDLG_H

#include <QDialog>
#include <QMessageBox>

namespace Ui {
class AboutBlg;
}

class AboutDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AboutDlg(QWidget *parent = nullptr);
    ~AboutDlg();

private slots:
    void on_qtBtn_clicked();

private:
    Ui::AboutBlg *ui;
};

#endif // ABOUTBLG_H
