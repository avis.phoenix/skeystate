#include "menuwindow.h"
#include <QApplication>

static MenuWindow *ventana = nullptr;

void pressKey(DWORD code, BOOL caps, BOOL shift)
{

    if (ventana != nullptr)
    {
        ventana->pressKey(code,caps,shift);
    }
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MenuWindow w;
    ventana = &w;

    WindowsKeyboardHook hook;
    WindowsKeyboardHook::callbackFunc=&pressKey;

    if (!hook.initHook())
    {
        if ( QMessageBox::critical(&w,w.tr("Error"),w.tr("Cannot inicialize Keyboard Hook")) == QMessageBox::Ok) {
            w.close();
            ventana = nullptr;
        }
    }

    return a.exec();
}
