#include "aboutdlg.h"
#include "ui_aboutblg.h"
#include "menuwindow.h"

AboutDlg::AboutDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutBlg)
{
    ui->setupUi(this);
    setModal(true);
    ui->versionLbl->setText("v" + MenuWindow::version);
}

AboutDlg::~AboutDlg()
{
    delete ui;
}

void AboutDlg::on_qtBtn_clicked()
{
    QMessageBox::aboutQt(this,"SKeyState powered by Qt Framework");
}
