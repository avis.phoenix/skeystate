#ifndef MENUWINDOW_H
#define MENUWINDOW_H

#include <QDialog>
#include <QSystemTrayIcon>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QCloseEvent>
#include <QColor>
#include <QPainter>
#include <QSettings>
#include "colorbutton.h"
#include "windowskeyboardhook.h"
#include "aboutdlg.h"

namespace Ui {
class MenuWindow;
}

class MenuWindow : public QDialog
{
    Q_OBJECT

public:
    explicit MenuWindow(QWidget *parent = nullptr);
    ~MenuWindow() override;

    void setVisible(bool visible) override;

    void pressKey(DWORD code, BOOL caps, BOOL shift);

    static const QString version;

private slots:
    void changeOnColor(QColor color);
    void changeOffColor(QColor color);
    void changeKey(int index);
    void iconActivated(QSystemTrayIcon::ActivationReason reason);

    void on_aboutBtn_clicked();

protected:
    void closeEvent(QCloseEvent *event) override;
private:
    Ui::MenuWindow *ui;

    void createActions();
    void createTrayIcon();
    void createColorButtons();
    void setIcon(QColor);
    void setIcons();
    void setTitle();


    QPixmap renderIcon(QColor color);

    void updateTrayIcon();
    void updateTexts();
    void updateColorButtons();
    void setKeyState(int index);
    void setColorSettings(QString group);
    void getColorSettings(QString group);

    ColorButton *onBtn, *offBtn;

    QColor onClr, offClr;

    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *quitAction;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

    bool activatedKey;
    static const int CODE[];

};

#endif // MENUWINDOW_H
