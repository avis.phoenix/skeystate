/*-------------------------------------------------------------------------------------------------
        Definición del ColorButton
		Definition of ColorButton

		Avis Phoenix e-mail: avis.phoenix@gmail.com
--------------------------------------------------------------------------------------------------*/

#include "colorbutton.h"

ColorButton::ColorButton(QWidget *parent,int x, int y, int ancho, int alto): QAbstractButton(parent)
{
    setGeometry(x,y,ancho,alto);
    Colore = QColor(Qt::red);
    Col = QPixmap(QSize(ancho,alto));
    Col.fill(Colore);
    Show = new QLabel(this);
    Show->setGeometry(0,0,ancho,alto);
    Show->setScaledContents(true);
    Show->setPixmap(Col);
    Show->setFrameShape(QFrame::Panel);
    Show->setFrameShadow(QFrame::Plain);

    connect(this,&QAbstractButton::clicked,this,&ColorButton::OnClick);
}

void ColorButton::setColor(const QColor &color)
{
    Colore = color;
    Col.fill(color);
    Show->setPixmap(Col);
}

void ColorButton::OnClick()
{
    const QColor color = QColorDialog::getColor(Colore, this, tr("Select Color"));
    if (color.isValid())
    {
        setColor(color);
        emit changeColor(color);
    }
}
