# SKeyState

<img src="/Icon/skIcon.png"  width="64" height="64">

This is a program to show the state of the some specials keys (Num Locks, Cap Locks, Scroll Locks), and change the color of the icon in the task bar to alert you if the key was activated or not. This is a useful app in case of fail the keyboard or you have a keyboard without the alerts lights to do this task. 

SKeyState is released under the GPL license.
This projects using [keylogger-class](https://gitlab.com/avis.phoenix/keylogger-class).

This is the v1.0 rc

This project was inspirated in [CapsCheck](http://2ddentertainment.com/CapsCheck/capscheck.htm) project.

[Exec Download link](SKeyState.zip).

# TO-DO
- [ ] Have an Installer
- [X] ~~Have a config save file~~ Save the colors configuration for each key
- [X] Save the last key selected
- [ ] Document the code

# Compile configuration

This projects was created in C++ with Qt 5.12.3 and the MSVC2015 compiler.

# How to use

The application stay all the time in the task bar (you can fix the app always visible):
![Tray Icon](/screenshots/trayicon.jpg)

with double click on the icon (circle green or black) show the options window:
![Optios Window](/screenshots/configWindow.jpg)


