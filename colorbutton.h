/*-------------------------------------------------------------------------------------------------
		Boton para la paleta de colores
		Button for the palette window

		Avis Phoenix e-mail: avis.phoenix@gmail.com
--------------------------------------------------------------------------------------------------*/

#include <QLabel>
#include <QAbstractButton>
#include <QColor>
#include <QPixmap>
#include <QColorDialog>

class ColorButton : public QAbstractButton
{
	Q_OBJECT
    public:
        explicit ColorButton(QWidget *parent,int x, int y, int alto, int ancho); //Define the button with parent, position, width and height
        QColor Color() {return Colore;} //return the actual color
        void setColor(const QColor &color); //Define the color of the button

    private slots:
        void OnClick();

    signals:
        void changeColor(QColor);
    protected:
        void paintEvent(QPaintEvent *) override {}
	private:

		QPixmap Col;
		QColor Colore;
		QLabel *Show;


};
