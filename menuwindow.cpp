#include "menuwindow.h"
#include "ui_menuwindow.h"
#include <QDebug>

const int MenuWindow::CODE[] = {VK_NUMLOCK, VK_CAPITAL, VK_SCROLL};
const QString MenuWindow::version = "1.0 rc";

MenuWindow::MenuWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MenuWindow)
{
    ui->setupUi(this);

    ui->hideMe->setVisible(false);
    ui->versionLbl->setText("v" + version);

    onClr.setRgb(0,255,0);
    offClr.setRgb(25,25,25);

    createActions();
    createTrayIcon();

    QSettings settings("avisphoenix", "SKeyState");
    int idx = settings.value("lastIndex", 0).toInt();
    if (idx <= ui->keyCbx->count()-1 && idx >=0)
    {
        ui->keyCbx->setCurrentIndex(idx);
    }
    getColorSettings(ui->keyCbx->currentText().remove(' '));
    setKeyState(ui->keyCbx->currentIndex());
    updateTexts();
    setIcons();

    createColorButtons();

    connect(ui->closeBtn,&QAbstractButton::clicked,this,&QWidget::hide);
    connect(ui->keyCbx,QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MenuWindow::changeKey);

    trayIcon->show();

    setFixedSize(330,190);

    hide();


}

MenuWindow::~MenuWindow()
{
    delete ui;
}

void MenuWindow::changeOnColor(QColor color)
{
    onClr = color;
    updateTrayIcon();
    setColorSettings(ui->keyCbx->currentText().remove(' '));
}

void MenuWindow::changeOffColor(QColor color)
{
    offClr = color;
    updateTrayIcon();
    setColorSettings(ui->keyCbx->currentText().remove(' '));
}

void MenuWindow::changeKey(int index)
{
    QString key = ui->keyCbx->itemText(index);
    setKeyState(index);
    getColorSettings(ui->keyCbx->currentText().remove(' '));
    QString state = activatedKey ? "ON" : "OFF";
    setWindowTitle("SKeyState " + key + " - " + state);
    trayIcon->setToolTip(key + " - " + state);
    updateTrayIcon();
    updateColorButtons();
}

void MenuWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick)
    {
        //qDebug() << "Doble click";
        if (isVisible())
        {
            hide();
            //qDebug() << "hide";
        }
        else
        {
            show();
            //qDebug() << "show";
        }
    }
}

void MenuWindow::setVisible(bool visible)
{
    minimizeAction->setEnabled(visible);
    restoreAction->setEnabled(!visible);
    QDialog::setVisible(visible);
}

void MenuWindow::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible()) {
        QSettings settings("avisphoenix", "SKeyState");
        bool first = settings.value("FirstTime",true).toBool();
        if (first)
        {
            QMessageBox::information(this, "SKeyState",
                                 tr("The program will keep running in the "
                                    "system tray. To terminate the program, "
                                    "choose <b>Quit</b> in the context menu "
                                    "of the system tray entry."));
            settings.setValue("FirstTime",false);
        }
        hide();
        event->ignore();
    }
}

void MenuWindow::createActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
}

void MenuWindow::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);

    connect(trayIcon, &QSystemTrayIcon::activated, this, &MenuWindow::iconActivated);
}

void MenuWindow::createColorButtons()
{
    onBtn = new ColorButton(ui->onGbx,62,24,32,32);
    onBtn->setColor(onClr);

    offBtn = new ColorButton(ui->OffGbx,62,24,32,32);
    offBtn->setColor(offClr);

    connect(onBtn,&ColorButton::changeColor, this, &MenuWindow::changeOnColor);
    connect(offBtn,&ColorButton::changeColor, this, &MenuWindow::changeOffColor);
}

void MenuWindow::setIcon(QColor color)
{
    trayIcon->setIcon(QIcon(renderIcon(color)));
}

void MenuWindow::setIcons()
{
    QColor colorIcon;

    if (activatedKey)
    {
        colorIcon = onClr;
    }
    else
    {
        colorIcon = offClr;
    }
    setWindowIcon(QIcon(":/Icon/skIcon.png"));
    trayIcon->setIcon(QIcon(renderIcon(colorIcon)));
}

QPixmap MenuWindow::renderIcon(QColor color)
{
    QPixmap icon(128,128);
    icon.fill(Qt::transparent);

    QPainter pintar(&icon);
    pintar.setBrush(color);

    pintar.drawEllipse(0,0,128,128);

    pintar.end();

    return icon;
}

void MenuWindow::updateTrayIcon()
{
    if (activatedKey)
    {
        trayIcon->setIcon(QIcon(renderIcon(onClr)));
    }
    else
    {
        trayIcon->setIcon(QIcon(renderIcon(offClr)));
    }
}

void MenuWindow::updateTexts()
{
    QString key = ui->keyCbx->currentText();
    QString state = activatedKey ? "ON" : "OFF";
    setWindowTitle("SKeyState " + key + " - " + state);
    trayIcon->setToolTip(key + " - " + state);
}

void MenuWindow::updateColorButtons()
{
    onBtn->setColor(onClr);
    offBtn->setColor(offClr);
}

void MenuWindow::setKeyState(int index)
{
    activatedKey = WindowsKeyboardHook::getKeyState(CODE[index]);
    //qDebug() << "activatedKey =" << (activatedKey ? "ON" : "OFF") << "output = " << GetKeyState(VK_NUMLOCK) << " whk=" << (WindowsKeyboardHook::getKeyState(CODE[index]) ? "ON" : "OFF");
}

void MenuWindow::pressKey(DWORD code, BOOL , BOOL )
{
    if (code == (DWORD)CODE[ui->keyCbx->currentIndex()])
    {
        activatedKey = !activatedKey;
        //qDebug() << "activatedKey :=" << (activatedKey ? "ON" : "OFF");
        updateTrayIcon();
        updateTexts();
    }
}

void MenuWindow::on_aboutBtn_clicked()
{
    AboutDlg *dlg = new AboutDlg(this);

    dlg->show();
}

void MenuWindow::setColorSettings(QString group)
{
    QSettings settings("avisphoenix", "SKeyState");
    settings.beginGroup(group);
    settings.setValue("onColor", onClr);
    settings.setValue("offColor", offClr);
    settings.endGroup();
}

void MenuWindow::getColorSettings(QString group)
{
    QSettings settings("avisphoenix", "SKeyState");
    settings.beginGroup(group);
    onClr = settings.value("onColor",QColor(0,255,0)).value<QColor>();
    offClr = settings.value("offColor",QColor(25,25,25)).value<QColor>();
    settings.endGroup();
}
